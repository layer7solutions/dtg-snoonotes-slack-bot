package main

import (
	"encoding/json"
	"fmt"
	"time"

	sn "bitbucket.org/layer7solutions/snoonotes"

	"github.com/nlopes/slack"
	"github.com/pkg/errors"
)

func getUserNotes(slackUID, target string) (*slack.Msg, error) {
	l := log.WithField("prefix", "getUserNotes")

	user, okUser := slackUsers.GetName(slackUID)
	if !okUser {
		l.WithField("UID", slackUID).Warn("unknown user")
		return &slack.Msg{
			Attachments: []slack.Attachment{{
				Color:    "danger",
				Fallback: "You are not signed up. Please use /snregister",
				Title:    "You are not signed up",
				Text:     "Please sign up using the /snregister command, and try again.",
				Footer:   "If this error keeps occuring, complain to ttgmpsn",
			}},
		}, errors.New("you shouldn't be seeing this")
	}

	l = l.WithField("user", user)

	if target == "" {
		return nil, errors.New("no or invalid username provided")
	}

	l.WithField("target", target).Debug("getting notes")

	types, err := sn.GetNoteTypeMap(user, sn.Subreddit)
	if err != nil {
		return nil, errors.Wrap(err, "error getting note type map")
	}

	// fill with default ("no notes found") attachment, to be replaced later
	ret := &slack.Msg{
		Text: fmt.Sprintf("SnooNotes for <https://www.reddit.com/user/%s|/u/%s>:", target, target),
		Attachments: []slack.Attachment{{
			Fallback: "No notes found",
			Text:     "No notes found",
		}},
	}

	notes, err := sn.Get(user, target)
	if err != nil {
		return nil, errors.Wrap(err, "error getting notes")
	}
	// No notes found
	if notes == nil {
		return ret, nil
	}

	var attachments []slack.Attachment
	for _, note := range *notes {
		noteType, ok := types[note.NoteTypeID]
		if !ok {
			noteType = sn.SimpleNoteType{
				DisplayName: "UNKNOWN (TELL SOMEBODY!)",
				ColorCode:   "000",
			}
		}
		var t time.Time
		t, err = time.Parse(time.RFC3339Nano, note.TimeStamp)
		if err != nil {
			t = time.Now()
		}
		attachments = append(attachments, slack.Attachment{
			Color:    fmt.Sprintf("%s", noteType.ColorCode),
			Fallback: fmt.Sprintf("%s by /u/%s on %s: %s", noteType.DisplayName, insertZWS(note.Submitter), t.Format(time.RFC1123), note.Message),

			Title:     note.Message,
			TitleLink: note.URL,

			Footer:     fmt.Sprintf("*%s* by <https://www.reddit.com/user/%s|%s>", noteType.DisplayName, note.Submitter, insertZWS(note.Submitter)),
			FooterIcon: "https://snoonotes.com/favicon.ico",

			Ts: json.Number(fmt.Sprintf("%d", t.Unix())),
		})
	}

	ret.Attachments = attachments

	return ret, nil
}
