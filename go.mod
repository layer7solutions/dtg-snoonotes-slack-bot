module DTG-SnooSnoo

require (
	bitbucket.org/layer7solutions/snoonotes v0.0.0
	github.com/BurntSushi/toml v0.3.1
	github.com/beefsack/go-rate v0.0.0-20180408011153-efa7637bb9b6 // indirect
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jzelinskie/geddit v0.0.0-20190913104144-95ef6806b073
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/nlopes/slack v0.6.1-0.20191106133607-d06c2a2b3249
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/thecsw/mira v3.0.0+incompatible
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/crypto v0.0.0-20191002192127-34f69633bfdc // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	mellium.im/sasl v0.2.1 // indirect
)

replace bitbucket.org/layer7solutions/snoonotes => ./snoonotes
