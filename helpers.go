package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"unicode/utf8"

	sn "bitbucket.org/layer7solutions/snoonotes"

	"github.com/go-pg/pg"
	"github.com/nlopes/slack"
	"github.com/pkg/errors"
	"github.com/thecsw/mira"
)

var userRegexp = regexp.MustCompile(`(?:/?(?:user|u)/)?([\w-]{3,20})(?:/\w+)?`)
var redditRegexps = [...]*regexp.Regexp{
	// tried from top to bottom
	// http://redd.it/85ucwb
	regexp.MustCompile(`^https?://(?:redd\.it|reddit\.com)/(?P<post>[[:alnum:]]+)`),
	// https://www.reddit.com/tb/85ucwb
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/tb/(?P<post>[[:alnum:]]+)`),
	// MODMAIL: https://www.reddit.com/message/messages/b8xdaj
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/message/messages/(?P<modmail>[[:alnum:]]+)`),
	// MODMAIL: https://mod.reddit.com/mail/all/3ba6u
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/mail/[[:alnum:]]+/(?P<modmailnew>[[:alnum:]]+)`),
	// https://www.reddit.com/r/DestinyTheGame/comments/85ucwb/d2_weekly_reset_thread_20180320/dw03oet/
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/(?:r/[[:alnum:]]+/)?comments/[[:alnum:]]+/(?:[\w-_.]+)?/(?P<comment>[[:alnum:]]+)`),
	// https://www.reddit.com/r/DestinyTheGame/comments/85ucwb/d2_weekly_reset_thread_20180320/
	regexp.MustCompile(`^https?://(?:\w+\.)?reddit\.com/(?:r/[[:alnum:]]+/)?comments/(?P<post>[[:alnum:]]+)`),
}

/*
This function returns the reddit username for a variety of formats.
Supported are:
	"/u/Oryxhasnonuts",
	"/user/Oryxhasnonuts",
	"/user/Oryxhasnonuts/",
	"user/Oryxhasnonuts",
	"u/Oryxhasnonuts",
	"Oryxhasnonuts",
	"/Oryxhasnonuts",
	"https://mobilesucks.redd.it/user/Oryxhasnonuts/",
	"http://anyfuckingdomain.doicare/user/Oryxhasnonuts/",
	"https://www.reddit.com/user/Oryxhasnonuts/",
	"https://www.reddit.com/user/Oryxhasnonuts/overview",
*/

func parseUser(user string) string {
	check := user
	if u, err := url.Parse(user); err == nil {
		check = u.Path
	}
	check = strings.TrimPrefix(check, "reddit.com")
	if strings.HasPrefix(check, "/r/") || strings.HasPrefix(check, "r/") {
		return ""
	}
	m := userRegexp.FindStringSubmatch(check)

	if len(m) != 2 {
		return ""
	}

	return m[1]
}

func parseURL(url string) string {
	var thing string
	url = strings.Trim(url, "<>")

	isModMail := false

	for _, r := range redditRegexps {
		m := r.FindStringSubmatch(url)
		if len(m) != 2 {
			continue
		}
		switch r.SubexpNames()[1] {
		case "post":
			thing = "t3_" + m[1]
		case "comment":
			thing = "t1_" + m[1]
		case "modmail":
			isModMail = true
			thing = "t4_" + m[1]
		case "modmailnew":
			isModMail = true
			thing = "ModmailMessage_" + m[1]
		default:
			panic("redditRegexp: unknown type")
		}
		break
	}
	if thing == "" {
		return ""
	}

	var query skyNetThingGetter
	if isModMail {
		thingObj := skyNetModmail{ID: thing}
		if err := db.Select(&thingObj); err != nil {
			log.WithField("prefix", "parseURL").WithError(err).Warnf("couldn't find author for modmail %s", thing)
			if err != pg.ErrNoRows {
				return ""
			}
		}
		query = thingObj
	} else {
		thingObj := skyNetThing{ID: thing}
		if err := db.Select(&thingObj); err != nil {
			log.WithField("prefix", "parseURL").WithError(err).Warnf("couldn't find author for submission %s", thing)
			if err != pg.ErrNoRows {
				return ""
			}
		}
		query = thingObj
	}

	if isModMail || query.GetName() != "" {
		if query.GetSubredditID() != skyNetSubredditID {
			return ""
		}
		return query.GetName()
	}

	// Try to get via reddit api
	log.WithField("prefix", "parseURL").Debugf("trying via reddit api for thing %s", thing)
	var thingR *mira.Reddit
	thingR = reddit.Submission(thing)
	/*if strings.HasPrefix(thing, "t3_") {
		thingR = reddit.Submission(thing)
	} else if strings.HasPrefix(thing, "t1_") {
		thingR = reddit.Comment(thing)
	} else {
		return ""
	}*/

	var i mira.MiraInterface
	var err error
	if i, err = thingR.Info(); err != nil {
		log.WithField("prefix", "parseURL").WithError(err).Warnf("couldn't find author for submission %s via reddit api", thing)
		return ""

	}
	if i.GetSubreddit() != sn.Subreddit {
		return ""
	}
	return i.GetAuthor()
}

func slackSendResponse(url string, msg *slack.Msg) error {
	data, err := json.Marshal(msg)
	if err != nil {
		return errors.Wrap(err, "json marshal")
	}

	var req *http.Request
	req, err = http.NewRequest("POST", url, bytes.NewReader(data))
	if err != nil {
		return errors.Wrap(err, "creating request failed")
	}
	req.Header.Set("Content-Type", "application/json")
	client := http.DefaultClient

	_, err = client.Do(req)
	if err != nil {
		return errors.Wrap(err, "http request failed")
	}

	return nil
}

type skyNetThingGetter interface {
	GetSubredditID() int
	GetName() string
}

type skyNetThing struct {
	tableName   struct{} `sql:"reddit_thing"`
	ID          string   `sql:"thing_id,pk"`
	SubredditID int      `sql:"subreddit_id"`
	Author      string   `sql:"author"`
}

// GetSubredditID returns the SubredditID
func (s skyNetThing) GetSubredditID() int {
	return s.SubredditID
}

// GetName returns the Redditor Name
func (s skyNetThing) GetName() string {
	return s.Author
}

type skyNetModmail struct {
	tableName   struct{} `sql:"modmail"`
	ID          string   `sql:"thing_id,pk"`
	SubredditID int      `sql:"subreddit_id"`
	MessageFrom string   `sql:"message_from"`
}

// GetSubredditID returns the SubredditID
func (s skyNetModmail) GetSubredditID() int {
	return s.SubredditID
}

// GetName returns the Redditor Name
func (s skyNetModmail) GetName() string {
	return s.MessageFrom
}

func insertZWS(s string) string {
	// https://www.fileformat.info/info/unicode/char/200B/index.htm
	zws, _ := utf8.DecodeRune([]byte("\xE2\x80\x8B"))
	var buffer bytes.Buffer
	for i, rune := range s {
		buffer.WriteRune(rune)
		if i%2 == 0 {
			buffer.WriteRune(zws)
		}
	}
	return buffer.String()
}
