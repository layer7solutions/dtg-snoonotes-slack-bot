package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	sn "bitbucket.org/layer7solutions/snoonotes"

	"github.com/nlopes/slack"
	"github.com/pkg/errors"
)

func handleInteractive(w http.ResponseWriter, r *http.Request) {
	l := log.WithField("prefix", "interactive")

	if err := r.ParseForm(); err != nil {
		l.WithError(err).Error("couldn't parse form")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	payload := r.PostFormValue("payload")
	if payload == "" {
		l.Error("empty payload")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var i slack.InteractionCallback

	if err := json.Unmarshal([]byte(payload), &i); err != nil {
		l.WithError(err).Error("can't unmarshal json")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if i.Token != config.Slack.VerificationToken {
		l.Error("invalid verification token")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	//l.Debugf("received %#v", i)

	l = l.WithField("callback_id", i.CallbackID)

	switch i.CallbackID {
	case "cat_select":
		w.WriteHeader(http.StatusOK)
		message, err := func() (*slack.Msg, error) {
			state, okState := addNoteStates.Get(i.User.ID)
			if !okState || state.NoteType != 0 {
				return nil, errors.New("invalid state")
			}
			l = l.WithField("user", state.User)

			var actionValue string
			for _, aa := range i.ActionCallback.AttachmentActions {
				// cancel thing
				if aa.Name == "cancel" && aa.Value == "cancel" {
					addNoteStates.Del(i.User.ID)
					return &slack.Msg{DeleteOriginal: true}, nil
				}
				if aa.Name != "cat_list" {
					continue
				}
				if len(aa.SelectedOptions) == 1 {
					actionValue = aa.SelectedOptions[0].Value
				}
			}
			if actionValue == "" {
				return nil, errors.New("no actionValue provided")
			}
			actionValueInt, err := strconv.Atoi(actionValue)
			if err != nil {
				return nil, errors.Wrap(err, "couldn't convert actionValue to int")
			}

			types, _ := sn.GetNoteTypeMap(state.User, sn.Subreddit)
			noteType, ok := types[actionValueInt]
			if !ok {
				noteType = sn.SimpleNoteType{
					DisplayName: "UNKNOWN (TELL SOMEBODY!)",
					ColorCode:   "000",
				}
			}

			state.NoteType = actionValueInt
			addNoteStates.Set(i.User.ID, state)

			return &slack.Msg{
				Text:            i.OriginalMessage.Text,
				ReplaceOriginal: true,
				Attachments: []slack.Attachment{{
					Color: fmt.Sprintf("%s", noteType.ColorCode),
					Text:  fmt.Sprintf("Selected Category: %s\nEnter Note Text:", noteType.DisplayName),
				}},
			}, nil
		}()

		if err != nil {
			l.WithError(err).Warn("cat_select failed")
			message = &slack.Msg{
				Attachments: []slack.Attachment{{
					Color:    "danger",
					Fallback: fmt.Sprintf("Error selecting category: %s", err),
					Title:    "Error selecting category!",
					Text:     fmt.Sprintf("Error: %s", err),
					Footer:   "If this error keeps occuring, complain to ttgmpsn",
				}},
			}
		}
		if err = slackSendResponse(i.ResponseURL, message); err != nil {
			l.WithError(err).Error("sending data to ResponseURL")
			return
		}
		l.Debug("Sent Category response")
	case "note_submit":
		w.WriteHeader(http.StatusOK)
		message, err := func() (*slack.Msg, error) {
			state, okState := addNoteStates.Get(i.User.ID)
			if !okState || state.NoteType == 0 || state.NoteText == "" {
				return nil, errors.New("invalid state")
			}
			// delete state (Yeah here, doesn't hurt)
			addNoteStates.Del(i.User.ID)

			var actionValue string
			for _, aa := range i.ActionCallback.AttachmentActions {
				if aa.Name != "okay" {
					continue
				}
				actionValue = aa.Value
			}
			if actionValue == "" {
				return nil, errors.New("no actionValue provided")
			}
			if actionValue == "no" {
				return &slack.Msg{DeleteOriginal: true}, nil
			}

			types, _ := sn.GetNoteTypeMap(state.User, sn.Subreddit)
			noteType, ok := types[state.NoteType]
			if !ok {
				noteType = sn.SimpleNoteType{
					DisplayName: "UNKNOWN (TELL SOMEBODY!)",
					ColorCode:   "000",
				}
			}

			note := sn.Note{
				NoteTypeID:        state.NoteType,
				SubName:           sn.Subreddit,
				Message:           state.NoteText,
				AppliesToUsername: state.Target,
				URL:               state.NoteURL,
			}
			l.Debugf("submitting note %#v", note)
			if err := sn.Add(state.User, note); err != nil {
				return nil, err
			}

			return &slack.Msg{
				Text:            fmt.Sprintf("Added new SnooNote for <https://www.reddit.com/user/%s|/u/%s>:", state.Target, state.Target),
				ReplaceOriginal: true,
				Attachments: []slack.Attachment{{
					Color: fmt.Sprintf("%s", noteType.ColorCode),

					Title:     state.NoteText,
					TitleLink: state.NoteURL,

					Footer:     fmt.Sprintf("*%s* by <https://www.reddit.com/user/%s|%s>", noteType.DisplayName, state.User, state.User),
					FooterIcon: "https://snoonotes.com/favicon.ico",

					Ts: json.Number(fmt.Sprintf("%d", time.Now().Unix())),
				}},
			}, nil
		}()

		if err != nil {
			l.WithError(err).Warn("note_submit failed")
			message = &slack.Msg{
				Attachments: []slack.Attachment{{
					Color:    "danger",
					Fallback: fmt.Sprintf("Error submitting note: %s", err),
					Title:    "Error submitting note!",
					Text:     fmt.Sprintf("Error: %s", err),
					Footer:   "If this error keeps occuring, complain to ttgmpsn",
				}},
			}
		}
		if err = slackSendResponse(i.ResponseURL, message); err != nil {
			l.WithError(err).Error("sending data to ResponseURL")
			return
		}
		l.Debug("Sent NoteSubmit response")
	case "snsnote":
		w.WriteHeader(http.StatusOK)
		message := func() *slack.Msg {
			var user string
			for _, aa := range i.ActionCallback.AttachmentActions {
				// "Hide"
				if aa.Name != "action" {
					continue
				}
				if aa.Value == "d" {
					return &slack.Msg{DeleteOriginal: true}
				}
				user = aa.Value
			}
			message, err := getUserNotes(i.User.ID, user)
			if err != nil {
				/*if message == nil {
					l.WithError(err).Error("failed to get snoonotes")
					message = &slack.Msg{
						Attachments: []slack.Attachment{{
							Color:    "danger",
							Fallback: fmt.Sprintf("Could not get SnooNotes: %s", err),
							Title:    "Could not fetch SnooNotes",
							Text:     fmt.Sprintf("Error: %s", err),
							Footer:   "If this error keeps occuring, complain to ttgmpsn",
						}},
					}
				}*/
				return nil
			}
			return message
		}()
		if message == nil {
			return
		}
		go slackAPI.PostMessage(i.Channel.ID, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
			slack.MsgOptionText(message.Text, false), slack.MsgOptionAttachments(message.Attachments...))
		if err := slackSendResponse(i.ResponseURL, &slack.Msg{DeleteOriginal: true}); err != nil {
			l.WithError(err).Error("sending data to ResponseURL")
			return
		}
	default:
		l.Warnf("unknown interactive callback")
		w.WriteHeader(http.StatusNotFound)
		return
	}
}
