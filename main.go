package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/go-pg/pg"
	"github.com/nlopes/slack"
	"github.com/sirupsen/logrus"
	"github.com/thecsw/mira"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

/*
TODO:
    - delete
    - delete failed login users?
*/

const controlChannel = "C2W3NV50A"
const skyNetSubredditID = 9073945

type slackConfig struct {
	ListenHost string
	ListenPort int
	LogFile    string

	Slack struct {
		VerificationToken string
		BotUserToken      string
	}

	Database struct {
		Username string
		Password string
		Host     string
		Database string
	}

	Reddit mira.Credentials
}

var config slackConfig
var db *pg.DB

var slackAPI *slack.Client
var slackInfo *slack.AuthTestResponse
var addNoteStates addNoteStatesStruct
var slackUsers usersStruct

var reddit *mira.Reddit

func init() {
	logrus.SetFormatter(&prefixed.TextFormatter{})
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(os.Stdout)

	addNoteStates = addNoteStatesStruct{s: make(map[string]addNoteState)}
	slackUsers = usersStruct{u: make(map[string]snUser)}
}

var log = logrus.WithField("prefix", "main")

func main() {
	var logfile, conffile string
	flag.StringVar(&logfile, "log", "", "path to a logfile (optional, stdout & debug enabled when skipped)")
	flag.StringVar(&conffile, "conf", "", "path to a configfile (required)")
	flag.Parse()

	if logfile != "" {
		f, err := os.OpenFile(logfile, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0660)
		if err != nil {
			panic("error opening log file")
		}
		defer f.Close()
		logrus.SetOutput(f)
		logrus.SetLevel(logrus.InfoLevel)
	}

	var err error

	log.Info("Starting DTG Snoonotes Slack Bot")
	log.Info("Reading config")
	if len(conffile) == 0 {
		log.Fatal("Please set the conffile flag")
	}
	if _, err = toml.DecodeFile(conffile, &config); err != nil {
		log.WithError(err).WithField("file", conffile).Fatal("reading config")
	}

	db = pg.Connect(&pg.Options{
		User:     config.Database.Username,
		Password: config.Database.Password,
		Database: config.Database.Database,
		Addr:     config.Database.Host,
	})

	// listen to slack
	log.Info("Setting up slack RTM listener")
	slackAPI = slack.New(
		config.Slack.BotUserToken,
		slack.OptionDebug(false),
	)

	slackInfo, err = slackAPI.AuthTest()
	if err != nil {
		log.WithError(err).Fatal("Slack Auth failed")
	}
	log.Infof("Connected to Slack as %s (ID %s)", slackInfo.User, slackInfo.UserID)

	var users []snUser
	if err = db.Model(&users).Select(); err != nil {
		panic(err)
	}
	for _, user := range users {
		if err = slackUsers.Add(user); err != nil {
			slackAPI.PostMessage(controlChannel, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
				slack.MsgOptionText(fmt.Sprintf("*WARNING*: Snoonote login failed for User <@%s> (/u/%s)", user.SlackUID, user.Name), false))
		}
	}

	// Login to Reddit
	reddit, err = mira.Init(config.Reddit)
	if err != nil {
		panic(err)
	}

	rtm := slackAPI.NewRTM()
	go rtm.ManageConnection()
	go func() {
		l := log.WithField("prefix", "slackMessage")
		for msg := range rtm.IncomingEvents {
			v, ok := msg.Data.(*slack.MessageEvent)
			if !ok {
				continue
			}
			// only direct messages
			if string(v.Msg.Channel[0]) != "D" {
				continue
			}
			// no user or text?!
			if len(v.Msg.User) == 0 || len(v.Msg.Text) == 0 {
				continue
			}
			// ignore myself, I talk too much
			if v.Msg.User == slackInfo.UserID {
				continue
			}
			// ignore slackbot, it sucks
			if v.Msg.User == "USLACKBOT" {
				continue
			}

			//log.Debugf("Got %#v", v.Msg)

			// safetynet if state goes wrong
			if v.Msg.Text == "SNOORESET" {
				l.Warnf("got SNOORESET from User %s", v.Msg.User)
				addNoteStates.Del(v.Msg.User)
				continue
			}

			if _, okUser := slackUsers.GetName(v.Msg.User); !okUser {
				l.WithField("UID", v.Msg.User).Warn("unknown user")
				slackAPI.PostMessage(v.Msg.Channel, slack.MsgOptionAsUser(true), slack.MsgOptionUser(slackInfo.UserID), slack.MsgOptionDisableLinkUnfurl(),
					slack.MsgOptionAttachments(slack.Attachment{
						Color:    "danger",
						Fallback: "You are not signed up. Please use /snregister",
						Title:    "You are not signed up",
						Text:     "Please sign up using the /snregister command, and try again.",
						Footer:   "If this error keeps occuring, complain to ttgmpsn",
					}),
				)
				continue
			}

			// check if state exists
			state, okState := addNoteStates.Get(v.Msg.User)
			// state.NoteType is 0 if the user doesn't cancel the
			// "select category" prompt, but goes on to enter a new username
			// codename: "squeaks anti-fail mechanic"
			if !okState || state.NoteType == 0 {
				// Delete Category select
				if okState && state.SlackTS != "" {
					slackAPI.DeleteMessage(state.SlackCID, state.SlackTS)
				}
				go addNoteStart(v.Msg.User, v.Msg.Channel, strings.Split(v.Msg.Text, " ")[0])
				continue
			}
			go addNoteText(v.Msg.User, v.Msg.Text, state)
		}
	}()

	http.HandleFunc("/slash", handleSlash)
	http.HandleFunc("/interactive", handleInteractive)
	log.Info("Listening on ", fmt.Sprintf("%s:%d", config.ListenHost, config.ListenPort))
	log.Error(http.ListenAndServe(fmt.Sprintf("%s:%d", config.ListenHost, config.ListenPort), nil))
}
