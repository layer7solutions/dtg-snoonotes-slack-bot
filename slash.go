package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/nlopes/slack"
	"github.com/pkg/errors"
)

func handleSlash(w http.ResponseWriter, r *http.Request) {
	l := log.WithField("prefix", "slash")

	s, err := slack.SlashCommandParse(r)
	if err != nil {
		l.WithError(err).Error("couldn't parse command")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if !s.ValidateToken(config.Slack.VerificationToken) {
		l.Error("invalid verification token")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	//l.Debugf("received %#v", s)

	l = l.WithField("command", s.Command).WithField("UID", s.UserID)

	switch s.Command {
	case "/sn":
		w.WriteHeader(http.StatusOK)
		// Call in go func to not provoke any "took too long to respond"
		// messages. We're responding via the provided URL anyways.
		// NOTE: do NOT use w or r *AT ALL* inside the go routine.
		go func() {
			text := strings.TrimSpace(strings.Split(s.Text, " ")[0])
			user := parseURL(text)
			if user == "" {
				user = parseUser(text)
			}
			var message *slack.Msg
			message, err = getUserNotes(s.UserID, user)
			if err != nil {
				if message == nil {
					l.WithError(err).Error("failed to get snoonotes")
					message = &slack.Msg{
						Attachments: []slack.Attachment{{
							Color:    "danger",
							Fallback: fmt.Sprintf("Could not get SnooNotes: %s", err),
							Title:    "Could not fetch SnooNotes",
							Text:     fmt.Sprintf("Error: %s", err),
							Footer:   "If this error keeps occuring, complain to ttgmpsn",
						}},
					}
				}
			} else {
				message.Attachments = append(message.Attachments, slack.Attachment{
					CallbackID: "snsnote",
					Ts:         json.Number(fmt.Sprintf("%d", time.Now().Unix())),
					Actions: []slack.AttachmentAction{
						{
							Name:  "action",
							Text:  "Post to Channel",
							Type:  "button",
							Value: user,
							Style: "primary",
						},
						{
							Name:  "action",
							Text:  "Hide",
							Type:  "button",
							Value: "d",       // min reddit username length is 3, there will never be a conflict.
							Style: "default", // danger will be red
						},
					},
				})
			}
			if err = slackSendResponse(s.ResponseURL, message); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
		}()
		return
	case "/snregister":
		w.WriteHeader(http.StatusOK)
		// Print Warning directly, no gofunc necessary -> quick
		if user, okUser := slackUsers.GetName(s.UserID); okUser {
			if err := slackSendResponse(s.ResponseURL, &slack.Msg{
				Attachments: []slack.Attachment{{
					Color:    "good",
					Fallback: fmt.Sprintf("You're already signed up as %s", user),
					Title:    "You are already registered!",
					Text:     fmt.Sprintf("Hey %s! Ask SkyNet for a second chance.", user),
				}},
			}); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
			return
		}
		if s.Text == "" {
			if err := slackSendResponse(s.ResponseURL, &slack.Msg{
				Attachments: []slack.Attachment{{
					Color:    "warning",
					Fallback: "go to snoonotes.com, login on the top right, go to \"user key\", request one, do /snregister RedditName key",
					Title:    "Help for /snregister:",
					Text:     "Go to snoonotes.com. On the top right, click Login, and authenticate with Reddit. Afterwards, click on \"User Key\" on snoonotes.com. Request a Key there. Then use */snregister RedditUsername Key* to sign up!",
					//Footer:   "If this error keeps occuring, complain to ttgmpsn",
				}},
			}); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
			return
		}
		go func() {
			err = func() error {
				str := strings.Split(s.Text, " ")
				if len(str) != 2 {
					return errors.New("only two arguments accepted: /snregister RedditName SnooKey\nUse /snregister without arguments for a help")
				}
				user := snUser{
					SlackUID: s.UserID,
					Name:     str[0],
					SNAPIKey: str[1],
				}
				if err = slackUsers.Add(user); err != nil {
					l.WithError(err).Error("failed SnooNotes auth")
					return errors.New("Auth failed - is your Name & Key correct?")
				}
				if err = db.Insert(&user); err != nil {
					l.WithError(err).Error("couldn't insert user into db")
					return errors.New("Couldn't store entry in the database")
				}
				return nil
			}()
			var message *slack.Msg
			if err != nil {
				message = &slack.Msg{
					Attachments: []slack.Attachment{{
						Color:    "danger",
						Fallback: fmt.Sprintf("Registration failed: %s", err),
						Title:    "Registration failed!",
						Text:     fmt.Sprintf("Error: %s", err),
						Footer:   "If this error keeps occuring, complain to ttgmpsn",
					}},
				}
			} else {
				message = &slack.Msg{
					Attachments: []slack.Attachment{{
						Color:    "good",
						Fallback: "You are now logged in!",
						Title:    "Registration successfull!",
						Text:     "SkyNet is happy to be at your service.",
						Footer:   "If this error keeps occuring, complain to ttgmpsn",
					}},
				}
			}
			if err = slackSendResponse(s.ResponseURL, message); err != nil {
				l.WithError(err).Error("sending data to ResponseURL")
				return
			}
		}()
		return
	case "/snhelp":
		w.WriteHeader(http.StatusOK)
		// Print info directly, no gofunc necessary -> quick
		if err := slackSendResponse(s.ResponseURL, &slack.Msg{
			Text: "Help for DeathBySnooSnoo:",
			Attachments: []slack.Attachment{
				{
					Color:    "warning",
					Fallback: "/sn username|link: View notes for that User",
					Title:    "Read SnooNotes: /sn user/link",
					Text:     "The output will first be sent to you privately. If you want to share it with the current channel, press the \"Post to Channel\" button. If you want to hide the result instead, press \"Hide\".",
				},
				{
					Color:    "warning",
					Fallback: "/msg @DeathBySnooSnoo username|link: Add a note for that User",
					Title:    "Adding notes: /msg @DeathBySnooSnoo username/link",
					Text:     "Add a note by pasting the username or link (preferred) into a DM with the bot. You'll then be interactively asked for a category and note text.",
					Footer:   "HINT: This can also be used to quickly check notes on mobile. Simply share a link into the DM channel, and cancel the category prompt!",
				},
				{
					Color:    "warning",
					Fallback: "/snregister Redditname Key: Register with the Bot",
					Title:    "Register with the Bot: /snregister RedditName Key",
					Text:     "Go to snoonotes.com. On the top right, click Login, and authenticate with Reddit. Afterwards, click on \"User Key\" on snoonotes.com. Request a Key there. Then use */snregister RedditUsername Key* to sign up!",
				},
			},
		}); err != nil {
			l.WithError(err).Error("sending data to ResponseURL")
			return
		}
	default:
		l.Warnf("unknown slash command")
		w.WriteHeader(http.StatusNotFound)
		return
	}
}
