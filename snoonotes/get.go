package snoonotes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
)

func Get(as, about string) (*[]note, error) {
	l := log.WithField("action", "Get").WithField("username", as).WithField("target", about)

	r, err := getAuthedRequest(as, "POST", "api/Note/GetNotes", strings.NewReader(fmt.Sprintf(`["%s"]`, about)))
	if err != nil {
		return nil, err
	}

	client := http.DefaultClient
	var resp *http.Response
	resp, err = client.Do(r)
	if err != nil {
		return nil, errors.Wrap(err, "http request failed")
	}
	defer safeClose(resp.Body)

	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "http body read failed")
	}

	var ns getNotes
	err = json.Unmarshal(body, &ns)
	if err != nil {
		return nil, errors.Wrap(err, "json unmarshal failed")
	}

	for k, n := range ns {
		if strings.EqualFold(k, about) {
			// filter out notes for one subreddit only!
			fn := n[:0]
			for _, sn := range n {
			    if sn.SubName == Subreddit {
			        fn = append(fn, sn)
			    }
			}
			l.Debugf("got %d (of %d) notes", len(fn), len(n))
			return &fn, nil
		}
	}

	l.Debug("no notes found")

	// no notes
	return nil, nil
}

var configCache struct {
	t time.Time
	s subSettings
}

func GetConfig(as, subname string) (*subSettings, error) {
	l := log.WithField("action", "GetConfig").WithField("username", as).WithField("sub", subname)

	if time.Now().Before(configCache.t) {
		l.Debugf("got cached, still valid until %s", configCache.t)
		return &configCache.s, nil
	}

	url := "restapi/Subreddit"
	if subname == "" {
		return nil, errors.New("can only fetch config for one subreddit at a time")
	}
	url += "/" + subname

	r, err := getAuthedRequest(as, "GET", url, nil)
	if err != nil {
		return nil, err
	}

	client := http.DefaultClient
	var resp *http.Response
	resp, err = client.Do(r)
	if err != nil {
		return nil, errors.Wrap(err, "http request failed")
	}
	defer safeClose(resp.Body)

	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "http body read failed")
	}

	var s []subSettings
	err = json.Unmarshal(body, &s)
	if err != nil {
		return nil, errors.Wrap(err, "json unmarshal failed")
	}

	for _, sub := range s {
		if strings.EqualFold(sub.SubName, subname) {
			l.Debug("got config")
			configCache.t = time.Now().Add(24 * time.Hour)
			configCache.s = sub
			return &sub, nil
		}
	}

	l.Warn("no config found")

	return nil, errors.New("no config found")
}

func GetNoteTypes(as, subname string) (*[]noteType, error) {
	l := log.WithField("action", "GetNoteTypes").WithField("username", as).WithField("sub", subname)

	settings, err := GetConfig(as, subname)
	if err != nil {
		l.WithError(err).Warn("couldn't get sub config")
		return nil, err
	}
	ret := []noteType{}
	for _, note := range settings.Settings.NoteTypes {
		if !strings.EqualFold(note.SubName, subname) {
			continue
		}
		ret = append(ret, note)
	}

	l.Debugf("found %d note types", len(ret))

	return &ret, nil
}

func GetNoteTypeMap(as, subname string) (map[int]SimpleNoteType, error) {
	l := log.WithField("action", "GetNoteTypeMap").WithField("username", as).WithField("sub", subname)

	notes, err := GetNoteTypes(as, subname)
	if err != nil {
		l.WithError(err).Warn("couldn't get note types")
		return nil, err
	}
	ret := make(map[int]SimpleNoteType)
	for _, note := range *notes {
		ret[note.NoteTypeID] = SimpleNoteType{note.DisplayName, note.ColorCode}
	}

	l.Debugf("found %d note types", len(ret))

	return ret, nil
}
