package snoonotes

import (
	"sync"
	"time"

	"github.com/pkg/errors"
)

type tokensStruct struct {
	sync.RWMutex
	t map[string]authToken
}

var ErrNoUser = errors.New("Unknown user")

func (t *tokensStruct) Set(user string, token authToken) {
	t.RLock()
	// if token is not changed, return
	if val, ok := t.t[user]; ok {
		if val == token {
			t.RUnlock()
			return
		}
	}
	t.RUnlock()

	t.Lock()
	t.t[user] = token
	t.Unlock()
}
func (t *tokensStruct) Get(user string) (authToken, error) {
	t.RLock()
	defer t.RUnlock()

	if val, ok := t.t[user]; ok {
		return val, nil
	}

	return authToken{}, ErrNoUser
}

// Response from auth/connect/token
type authToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   uint   `json:"expires_in"`
	TokenType   string `json:"token_type"`

	Expires time.Time `json:"-"`
	Key     string    `json:"-"`
}

// Response from api/Note/GetNotes
type getNotes map[string][]note

type note struct {
	NoteID          int
	NoteTypeID      int
	SubName         string
	Submitter       string
	Message         string
	URL             string `json:"Url"`
	TimeStamp       string
	ParentSubreddit *string
}

// Response from restapi/Subreddit
type subSettings struct {
	SubredditID int
	SubName     string
	Active      bool
	BotSettings *struct {
		DirtBagURL      string `json:"DirtbagUrl"`
		DirtbagUsername string
	}
	Settings struct {
		AccessMask int
		NoteTypes  []noteType
		PermBanID  *int
		TempBanID  *int
	}
}

type noteType struct {
	NoteTypeID   int
	SubName      string
	DisplayName  string
	ColorCode    string
	DisplayOrder int
	Bold         bool
	Italic       bool
}

type SimpleNoteType struct {
	DisplayName string
	ColorCode   string
}

// Request to api/note
type Note struct {
	NoteTypeID        int
	SubName           string
	Message           string
	AppliesToUsername string
	URL               string `json:"url"`
}
