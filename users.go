package main

import (
	"sync"

	sn "bitbucket.org/layer7solutions/snoonotes"
)

type snUser struct {
	tableName struct{} `sql:"snoonotes_slackbot"`
	SlackUID  string   `sql:"slack_id"`
	Name      string   `sql:"reddit_username"` // reddit username
	SNAPIKey  string   `sql:"api_key"`
}

type usersStruct struct {
	sync.RWMutex
	u map[string]snUser
}

func (u *usersStruct) Add(user snUser) error {
	if err := sn.Auth(user.Name, user.SNAPIKey); err != nil {
		return err
	}

	u.Lock()
	u.u[user.SlackUID] = user
	u.Unlock()

	return nil
}
func (u *usersStruct) GetName(slackUID string) (string, bool) {
	u.RLock()
	s, ok := u.u[slackUID]
	u.RUnlock()

	return s.Name, ok

}
